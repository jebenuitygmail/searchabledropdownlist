({
    clearOpts : function( component ) {
        window.setTimeout(
            $A.getCallback(function() {
                component.set('v.DDLOpts',[]);
            }), 100
        );
    }
})