({
    leaveFocus : function(component, event, helper){
        helper.clearOpts(component);
    },
    filterDDL : function(component, event, helper) {
        let ss = component.get('v.searchStr');

        let newDdlOpts = [];
        let DdlOpts = component.get('v.selectList');

        //let limit = 20;
        if( $A.util.isEmpty(ss) ) DdlOpts.forEach( opt => {
            //if( newDdlOpts.length < limit ){
                newDdlOpts.push(opt);
            //}
        });
        else DdlOpts.forEach( opt => {
            if( opt.label.toLowerCase().includes(ss.toLowerCase()) /*&& newDdlOpts.length < limit*/ ){
                newDdlOpts.push(opt);
            }
        });

        component.set('v.DDLOpts',newDdlOpts);
    },

    chooseOpt : function(component, event, helper){
        let divOpt = event.target.innerText;

        component.set('v.searchStr',divOpt);
        component.set('v.DDLOpts',[]);

        let DdlOpts = component.get('v.selectList');
        DdlOpts.forEach( opt => {
            if( opt.label === divOpt ){
                component.set('v.selectedValue',opt.value);
            }
        });
    }
})