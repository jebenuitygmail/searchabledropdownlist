({
    getPicklistValues : function( component ){
        let sobjName = component.get('v.SObjectAPIName');
        let plApiName = component.get('v.picklistFieldAPIName');

        let action = component.get('c.getPicklistOptions');

        action.setParams({
            ObjApiName : sobjName
            , fieldApiName : plApiName
        });

        action.setCallback(this, response => {
            let rOpts = response.getReturnValue();
            let rOptsMap = new Map(Object.entries(rOpts));
            let selectOpts = [];

            for (var [key, value] of rOptsMap) {
                selectOpts.push({label:value,value:key});
            };

            component.set('v.selectList',selectOpts);
        });

        $A.enqueueAction(action);
    }
})
