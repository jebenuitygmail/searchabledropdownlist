({
    doInit : function(component, event, helper) {
        let sobjName = component.get('v.SObjectAPIName');
        let plApiName = component.get('v.picklistFieldAPIName');

        if( $A.util.isEmpty(sobjName) || $A.util.isEmpty(plApiName) ){
            component.set('v.fatalError','There was a problem, contact your Admin');
            return;
        }

        helper.getPicklistValues(component);
    }
})
