public class PicklistSearchDDL {

    @AuraEnabled(cacheable=true)
    public static Map<String,String> getPicklistOptions( String ObjApiName, String fieldApiName ){
        SObjectType objSchema = Schema.getGlobalDescribe().get(ObjApiName);

        if( objSchema == null )
            throw new AuraHandledException('Couldn\'t find the Object schema by api name: ' + ObjApiName);

        Schema.SObjectField fieldSchema = objSchema.getDescribe().fields.getMap().get(fieldApiName);

        if( fieldSchema == null )
            throw new AuraHandledException('Couldn\'t find the field schema by api name: ' + fieldApiName);

        Schema.DescribeFieldResult fieldDescribe = fieldSchema.getDescribe();

        Schema.PicklistEntry[] pleArr = fieldDescribe.getPicklistValues();

        if( pleArr == null )
            throw new AuraHandledException('Couldn\'t find picklist options for field: ' + fieldApiName);

        Map<String,String> options = new Map<String,String>();
        for( Schema.PicklistEntry ple : pleArr ){
            options.put(
                ple.getValue()
                , ple.getLabel()
            );
        }

        return options;
    }

}
